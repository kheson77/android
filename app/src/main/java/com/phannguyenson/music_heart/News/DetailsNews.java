package com.phannguyenson.music_heart.News;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.phannguyenson.music_heart.R;
import com.squareup.picasso.Picasso;

public class DetailsNews extends AppCompatActivity {

    ImageView detailImg, back;
    TextView detailName;
    TextView detailID;
    TextView detailPrice;
    TextView detailCate;
    TextView detailDes;
    Button btnMua;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_news);

        anhXa();
        detail();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void anhXa(){
        detailImg  = findViewById(R.id.Image);
        detailName = findViewById(R.id.Name);
        detailID = findViewById(R.id.ID);
        detailPrice = findViewById(R.id.Price);
        detailCate = findViewById(R.id.Cate);
        detailDes = findViewById(R.id.Des);
        back = findViewById(R.id.back);
    }

    void detail(){
        Intent i = getIntent();
        int position = i.getIntExtra("Position", -1);
        detailName.setText(News.data.get(position).getTitle());
        detailID.setText("ID: " + News.data.get(position).getId());
        detailPrice.setText("Giá: " + News.data.get(position).getPrice());
        detailCate.setText("Phân loại: " + News.data.get(position).getCategory());
        detailDes.setText("Mô tả: " + News.data.get(position).getDescription());

        Picasso.with(this)
                .load(News.data.get(position).getImage())
                .resize(1000, 1000)
                .centerCrop()
                .into(detailImg);

        btnMua = findViewById(R.id.btnMua);
        btnMua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/search?q=men%27s+custom+clothing"));
                startActivity(i);
                finish();
            }
        });
    }
}