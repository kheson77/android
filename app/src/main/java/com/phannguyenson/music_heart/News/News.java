package com.phannguyenson.music_heart.News;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.phannguyenson.music_heart.MainActivity;
import com.phannguyenson.music_heart.Home.Home;
import com.phannguyenson.music_heart.News.Adapter.NewsAdapter;
import com.phannguyenson.music_heart.News.Model.NewsModel;
import com.phannguyenson.music_heart.Download;
import com.phannguyenson.music_heart.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class News extends AppCompatActivity {

    ListView lv;
    public static List<NewsModel> data;
    NewsAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        listview();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.news);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.news:
                        return true;
                    case R.id.music:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.download:
                        startActivity(new Intent(getApplicationContext(), Download.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

    }

    void listview(){
        lv = (ListView) findViewById(R.id.lv);
        data = new ArrayList<>();
        customAdapter = new NewsAdapter(this, data);
        lv.setAdapter(customAdapter);

        String url = "https://fakestoreapi.com/products";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Type listType = new TypeToken<List<NewsModel>>() {}.getType();
                        List<NewsModel> mylist = new Gson().fromJson(response, listType);
                        data.clear();
                        data.addAll(mylist);
                        customAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(News.this, "ERROR", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(News.this, DetailsNews.class);
                i.putExtra("Position", position);
                startActivity(i);
            }
        });
    }
}