package com.phannguyenson.music_heart.News.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phannguyenson.music_heart.News.Model.NewsModel;
import com.phannguyenson.music_heart.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends BaseAdapter {
    Context context;
    List<NewsModel> news;

    public NewsAdapter(Context context, List<NewsModel> product) {
        this.context = context;
        this.news = product;
    }

    @Override
    public int getCount() {
        return news.size();
    }

    @Override
    public Object getItem(int position) {
        return news.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;
        if(rowView == null){
            rowView = layoutInflater.inflate(R.layout.news_items, null, true);
        }
        ImageView Img = rowView.findViewById(R.id.Image);
        TextView Name = rowView.findViewById(R.id.Name);
        TextView ID = rowView.findViewById(R.id.ID);
        TextView Price = rowView.findViewById(R.id.Price);
        TextView Des = rowView.findViewById(R.id.Des);
        TextView Cate = rowView.findViewById(R.id.Cate);

        NewsModel itm = news.get(position);

        Picasso.with(context)
                .load(itm.getImage())
                .resize(1000, 1000)
                .centerCrop()
                .into(Img);

        Name.setText(itm.getTitle());
        ID.setText("ID: B17DCMM" + itm.getId());
        Price.setText("Giá: " + itm.getPrice() + "$");
        Des.setText("Mô tả: " + itm.getDescription());
        Cate.setText("Phân loại: " + itm.getCategory());

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale);
        rowView.startAnimation(animation);

        return rowView;
    }
}
